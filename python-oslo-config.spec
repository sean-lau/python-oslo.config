%global _empty_manifest_terminate_build 0
Name:           python-oslo-config
Version:        8.5.0
Release:        1
Summary:        Oslo Configuration API
License:        Apache-2.0
URL:            https://docs.openstack.org/oslo.config/latest/
Source0:        https://files.pythonhosted.org/packages/ef/ac/b9015ba65085e73971ec0caa34e99e0a5e16fd7e521274d5224507b004a4/oslo.config-8.5.0.tar.gz
BuildArch:      noarch
%description
Library The Oslo configuration API supports parsing command line arguments and

%package -n python3-oslo-config
Summary:        Oslo Configuration API
Provides:       python-oslo-config
# Base build requires
BuildRequires:  python3-devel
BuildRequires:  python3-setuptools
BuildRequires:  python3-pbr
BuildRequires:  python3-pip
BuildRequires:  python3-wheel
# General requires
BuildRequires:  python3-pyyaml
BuildRequires:  python3-debtcollector
BuildRequires:  python3-netaddr
BuildRequires:  python3-oslo-i18n
BuildRequires:  python3-requests
BuildRequires:  python3-rfc3986
BuildRequires:  python3-stevedore
BuildRequires:  python3-importlib-metadata
BuildRequires:  python3-sphinx
# Tests running requires
BuildRequires:  python3-bandit
BuildRequires:  python3-coverage
BuildRequires:  python3-fixtures
BuildRequires:  python3-hacking
BuildRequires:  python3-mypy
BuildRequires:  python3-oslo-log
BuildRequires:  python3-oslotest
BuildRequires:  python3-pre-commit
BuildRequires:  python3-requests-mock
BuildRequires:  python3-stestr
BuildRequires:  python3-testscenarios
BuildRequires:  python3-testtools
# General requires
Requires:       python3-pyyaml
Requires:       python3-debtcollector
Requires:       python3-netaddr
Requires:       python3-oslo-i18n
Requires:       python3-requests
Requires:       python3-rfc3986
Requires:       python3-stevedore
Requires:       python3-importlib-metadata
Requires:       python3-sphinx
# Tests running requires
Requires:       python3-bandit
Requires:       python3-coverage
Requires:       python3-fixtures
Requires:       python3-hacking
Requires:       python3-mypy
Requires:       python3-oslo-log
Requires:       python3-oslotest
Requires:       python3-pre-commit
Requires:       python3-requests-mock
Requires:       python3-stestr
Requires:       python3-testscenarios
Requires:       python3-testtools
%description -n python3-oslo-config
Library The Oslo configuration API supports parsing command line arguments and

%package help
Summary:        Oslo Configuration API
Provides:       python3-oslo-config-doc
%description help
Library The Oslo configuration API supports parsing command line arguments and

%prep
%autosetup -n oslo.config-8.5.0

%build
%py3_build

%install
%py3_install
install -d -m755 %{buildroot}/%{_pkgdocdir}
if [ -d doc ]; then cp -arf doc %{buildroot}/%{_pkgdocdir}; fi
if [ -d docs ]; then cp -arf docs %{buildroot}/%{_pkgdocdir}; fi
if [ -d example ]; then cp -arf example %{buildroot}/%{_pkgdocdir}; fi
if [ -d examples ]; then cp -arf examples %{buildroot}/%{_pkgdocdir}; fi
pushd %{buildroot}
if [ -d usr/lib ]; then
    find usr/lib -type f -printf "/%h/%f\n" >> filelist.lst
fi
if [ -d usr/lib64 ]; then
    find usr/lib64 -type f -printf "/%h/%f\n" >> filelist.lst
fi
if [ -d usr/bin ]; then
    find usr/bin -type f -printf "/%h/%f\n" >> filelist.lst
fi
if [ -d usr/sbin ]; then
    find usr/sbin -type f -printf "/%h/%f\n" >> filelist.lst
fi
touch doclist.lst
if [ -d usr/share/man ]; then
    find usr/share/man -type f -printf "/%h/%f.gz\n" >> doclist.lst
fi
popd
mv %{buildroot}/filelist.lst .
mv %{buildroot}/doclist.lst .

#%check
#%{__python3} setup.py test

%files -n python3-oslo-config -f filelist.lst
%dir %{python3_sitelib}/*


%files help -f doclist.lst
%{_docdir}/*

%changelog
* Mon Jul 12 2021 OpenStack_SIG <openstack@openeuler.org> - 8.5.0-1
- Upgrade version
* Fri Jan 22 2021 zhangy1317 <zhangy1317@foxmail.com>
- Add BuildRequires python3-pbr and python3-pip
* Fri Nov 20 2020 Python_Bot <Python_Bot@openeuler.org>
- Package Spec generated

